#include "NPC.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"

#include <stdexcept>
#include <string>

using std::string;

/**
 * NPC
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
NPC::NPC() : Sprite()
{
    state = IDLE;
    speed = 30.0f;

    maxRange = 0.4f;
    timeToTarget = 0.25f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;
}

/**
 * init
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void NPC::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/undeadking.png");

    //postion
    Vector2f position(300.0f,300.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 5, &position);

    // Setup the animation structure
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.4f);
    }
}

/**
 * ~NPC
 * 
 * Destroys the NPC and any associated 
 * objects 
 * 
 */
NPC::~NPC()
{

}

void NPC::update(float dt)
{
    ai();
    Sprite::update(dt);
}

/**
 * ai
 * 
 * Adjusts the velocity / state of the NPC
 * to follow the player
 * 
 */
void NPC::ai()
{
    // get player
    Player* player = game->getPlayer();

    // get distance to player
        // copy the player position
    Vector2f vectorToPlayer(player->getPosition());
        // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    float distance = vectorToPlayer.length();

    // if player 'in range' stop and fire
    if(distance < maxRange)
    {
        // put fire code in here later!
    }   
    else
    {
        // else - head for player

        // Could do with an assign in vector
        velocity->setX(vectorToPlayer.getX());
        velocity->setY(vectorToPlayer.getY());   
        
        // will work but 'wobbles'
        //velocity->scale(speed);   

        // Arrive pattern 
        velocity->scale(timeToTarget);

        if(velocity->length() > speed)
        {
            velocity->normalise();
            velocity->scale(speed);            
        }

        state = IDLE;

        if(velocity->getY() > 0.1f)
            state = DOWN;
        else
            if(velocity->getY() < -0.1f)
                state = UP;
            // empty else is not an error!
        
        if(velocity->getX() > 0.1f)
            state = RIGHT;
        else
            if(velocity->getX() < -0.1f)
                state = LEFT;
            // empty else is not an error. 
    }
}

void NPC::setGame(Game* game)
{
    this->game = game;
}

int NPC::getCurrentAnimationState()
{
    return state;
}
